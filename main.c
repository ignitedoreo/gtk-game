#include <gtk/gtk.h>
#include <stdlib.h>

GtkWidget *window;
GtkWidget *grid;
GtkWidget *button;
GtkWidget *label;

static void print_1 (GtkWidget *widget, gpointer data)
{
    int randf = rand();
    int randg = (randf % 4)+1;
    if (randg == 1) {
        gtk_label_set_label(GTK_LABEL(label), "You win!");
    } else {
        gtk_label_set_label(GTK_LABEL(label), "You loose, maybe next time!");
    }
}

static void print_2 (GtkWidget *widget, gpointer data)
{
    int randf = rand();
    int randg = (randf % 4)+1;
    if (randg == 2) {
        gtk_label_set_label(GTK_LABEL(label), "You win!");
    } else {
        gtk_label_set_label(GTK_LABEL(label), "You loose, maybe next time!");
    }
}
static void print_3 (GtkWidget *widget, gpointer data)
{
    int randf = rand();
    int randg = (randf % 4)+1;
    if (randg == 3) {
        gtk_label_set_label(GTK_LABEL(label), "You win!");
    } else {
        gtk_label_set_label(GTK_LABEL(label), "You loose, maybe next time!");
    }
}
static void print_4 (GtkWidget *widget, gpointer data)
{
    int randf = rand();
    int randg = (randf % 4)+1;
    if (randg == 4) {
        gtk_label_set_label(GTK_LABEL(label), "You win!");
    } else {
        gtk_label_set_label(GTK_LABEL(label), "You loose, maybe next time!");
    }
}

static void activate (GtkApplication *app, gpointer user_data)
{

    window = gtk_application_window_new (app);
    gtk_window_set_title (GTK_WINDOW (window), "Game");
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);

    grid = gtk_grid_new ();

    gtk_container_add (GTK_CONTAINER (window), grid);

    label = gtk_label_new("Guess number beetween 1-4");
    gtk_label_set_selectable(GTK_LABEL(label), FALSE);
    gtk_grid_attach (GTK_GRID(grid), label, 0, 0, 4, 1);

    button = gtk_button_new_with_label("1");
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(print_1), window);
    gtk_grid_attach(GTK_GRID(grid), button, 0, 1, 1, 1);

    button = gtk_button_new_with_label("2");
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(print_2), window);
    gtk_grid_attach(GTK_GRID(grid), button, 1, 1, 1, 1);

    button = gtk_button_new_with_label("3");
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(print_3), window);
    gtk_grid_attach(GTK_GRID(grid), button, 2, 1, 1, 1);

    button = gtk_button_new_with_label("4");
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(print_4), window);
    gtk_grid_attach(GTK_GRID(grid), button, 3, 1, 1, 1);

    // button = gtk_button_new_with_label ("Quit");

    // g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
 
    // gtk_grid_attach (GTK_GRID (grid), button, 1, 1, 1, 1);

    gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
    gtk_grid_set_column_spacing(GTK_GRID(grid), 4);
    gtk_widget_show_all (window);

}

int main (int argc, char **argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);
    g_object_unref (app);

    return status;
}